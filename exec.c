/*
 * This file is part of armexec.
 *
 * armexec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * armexec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with armexec.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <string.h>
#include <stdint.h>

#include <capstone/capstone.h>
#include <capstone/arm.h>

#include "armexec.h"

__thread uint32_t exec_curr_insn;

/* If its value is 0, always generate break */
static uint32_t exec_breakpoint = 0xffffffff;

static const uint32_t CAPSTON_REG_MAP[] = {
    [ARM_REG_R0]  = 0,
    [ARM_REG_R1]  = 1,
    [ARM_REG_R2]  = 2,
    [ARM_REG_R3]  = 3,
    [ARM_REG_R4]  = 4,
    [ARM_REG_R5]  = 5,
    [ARM_REG_R6]  = 6,
    [ARM_REG_R7]  = 7,
    [ARM_REG_R8]  = 8,
    [ARM_REG_R9]  = 9,
    [ARM_REG_R10] = 10,
    [ARM_REG_R11] = 11,
    [ARM_REG_R12] = 12,
    [ARM_REG_R13] = 13,
    [ARM_REG_R14] = 14,
    [ARM_REG_R15] = 15,
};

struct exec_env {
    uint32_t    core_registers[16];
    int         cpsr[4];
#define EXEC_MAX_FRAME  64
    exec_frame_t frames[EXEC_MAX_FRAME];
    int          fridx;
    csh          cp;
    int          init;
};

static __thread struct exec_env __exec_env;

#define FM      (__exec_env.frames)
#define FMI     (__exec_env.fridx)

/* argument/result/scratch */
#define R0      (__exec_env.core_registers[0])
#define R1      (__exec_env.core_registers[1])
/* argument/scratch */
#define R2      (__exec_env.core_registers[2])
#define R3      (__exec_env.core_registers[3])
/* variable */
#define R4      (__exec_env.core_registers[4])
#define R5      (__exec_env.core_registers[5])
#define R6      (__exec_env.core_registers[6])
#define R7      (__exec_env.core_registers[7])
#define R8      (__exec_env.core_registers[8])
/* platform */
#define R9      (__exec_env.core_registers[9])
/* variable */
#define R10     (__exec_env.core_registers[10])
#define R11     (__exec_env.core_registers[11])
/* IP, Intra-Procedure-call scratch */
#define R12     (__exec_env.core_registers[12])
#define IP      R12
/* SP, Stack Pointer */
#define R13     (__exec_env.core_registers[13])
#define SP      R13
/* LR, Link Register */
#define R14     (__exec_env.core_registers[14])
#define LR      R14
/* PC, Program Counter */
#define R15     (__exec_env.core_registers[15])
#define PC      R15

#define R(n)    (__exec_env.core_registers[(n)])
#define Rx(n)   R(CAPSTON_REG_MAP[(n)])

#define EXEC_ENV_INIT() do { \
    bzero(&__exec_env, sizeof(__exec_env)); \
} while (0)

#define N       (__exec_env.cpsr[0])
#define Z       (__exec_env.cpsr[1])
#define C       (__exec_env.cpsr[2])
#define V       (__exec_env.cpsr[3])

static int
cc_match(arm_cc cc)
{
    switch (cc) {
    case ARM_CC_INVALID:
        return 0;

    case ARM_CC_EQ:
        return Z == 1;

    case ARM_CC_NE:
        return Z == 0;

    case ARM_CC_HS:
        return C == 1;

    case ARM_CC_LO:
        return C == 0;

    case ARM_CC_MI:
        return N == 1;

    case ARM_CC_PL:
        return N == 0;

    case ARM_CC_VS:
        return V == 1;

    case ARM_CC_VC:
        return V == 0;

    case ARM_CC_HI:
        return C == 1 && Z == 0;

    case ARM_CC_LS:
        return C == 0 || Z == 1;

    case ARM_CC_GE:
        return N == V;

    case ARM_CC_LT:
        return N != V;

    case ARM_CC_GT:
        return Z == 0 && N == V;

    case ARM_CC_LE:
        return Z == 1 || N != V;

    case ARM_CC_AL:
        return 1;
    }

    return 0;
}

static void
setZ(uint32_t x)
{
    if (x == 0)
        Z = 1;
    else
        Z = 0;
}

static void
setN(uint32_t x)
{
    if (x & 0x08000000)
        N = 1;
    else
        N = 0;
}

static void
setC(uint32_t a, uint32_t b, uint32_t c)
{
    uint32_t rc;

    C = 0;
    rc = (a & 0x7FFFFFFF) + (b & 0x7FFFFFFF) + c; //carry in
    rc = (rc >> 31) + (a >> 31) + (b >> 31);  //carry out
    if (rc & 2)
        C = 1;
}

static void
setV(uint32_t a, uint32_t b, uint32_t c)
{
    uint32_t rc;
    uint32_t rd;

    V = 0;
    rc = (a & 0x7FFFFFFF) + (b & 0x7FFFFFFF) + c; //carry in
    rc >>= 31; //carry in in lsbit
    rd = (rc & 1) + ((a >> 31) & 1) + ((b >> 31) & 1); //carry out
    rd >>= 1; //carry out in lsbit
    rc = (rc ^ rd) & 1; //if carry in != carry out then signed overflow
    if(rc)
        V = 1;
}

static void
push_frame(char *name, uint32_t entry, uint32_t lr)
{
    char *fname = NULL;

    if (!name) {
        fname = malloc(64);
        snprintf(fname, 64, "sub_%x", entry);
    } else
        fname = strdup(name);

    if (FMI == EXEC_MAX_FRAME) {
        PANIC("Too deep frames\n");
    }
    FM[FMI].name = fname;
    FM[FMI].entry = entry;
    FM[FMI].lr = lr;
    FMI++;

    DEBUG("CALL  [%s]\n", fname);
}

static void
pop_frame(void)
{
    if (FMI > 0) {
        --FMI;
        DEBUG("LEAVE [%s]\n", FM[FMI].name);
        free(FM[FMI].name);
        FM[FMI].name = NULL;
    }
}

exec_frame_t *
exec_get_backtrace(int *depth)
{
    *depth = FMI;
    return FM;
}

/* return 0 if no stub found, otherwise return 1 */
static int
call_stub(struct armld *ld, uint32_t entry, uint32_t lr)
{
    elf_stub_t *stub;
    struct elf *elf = ld->elf;

    stub = elf_lookup_stub(elf, entry);

    if (!stub)
        return 0;

    if (stub->func) {
        uint32_t r0 = (stub->func)(ld, R0, R1, R2, R3, SP);
        R0 = r0;
    }

    return 1;
}

static void
sub_exec(csh cp,
         struct armld *ld,
         uint32_t start,
         uint32_t halt)
{
    int i;
    cs_arm *thm;
    arm_cc cc;
    uint8_t op_cnt;
    cs_arm_op *oprnds;
    arm_insn op_code;
    cs_insn *insn;
    uint32_t curr;
    const uint8_t *code;
    int stop_exec;
    struct vm *vm;
    struct elf *elf;

    vm = ld->vm;
    elf = ld->elf;

    curr = start;
    code = (uint8_t *) PA(curr);
    stop_exec = 0;

    do {
        /* gdb assistant */
        if (curr == exec_breakpoint || exec_breakpoint == 0)
            __breakpoint();

        if (curr == halt)
            stop_exec = 1;

        if (cs_disasm(cp, code, 4, curr, 1, &insn) != 1)
            break;

        exec_curr_insn = curr;

        DEBUG("%08x:\t%s\t\t%s\n",
               (uint32_t)insn->address,
               insn->mnemonic,
               insn->op_str);


        PC = insn->address;

        op_code = insn->id;
        thm = &insn->detail->arm;
        cc = thm->cc;
        op_cnt = thm->op_count;
        oprnds = thm->operands;

        for (i = 0; i < op_cnt; i++) {
            if (oprnds[i].shift.value) {
                /* shift is not executed! */
                PANIC("SHIFT: %d\n", oprnds[i].shift.value);
            }
        }

        switch (op_code) {
        case ARM_INS_AND:
        case ARM_INS_EOR:
        case ARM_INS_ORR:
        case ARM_INS_BIC: {
            /* 16 bits only has register variant */
            int rd, rm;
            uint32_t r = 0, a, b;

            rd = oprnds[0].reg;
            rm = oprnds[1].reg;

            a = Rx(rd);
            b = Rx(rm);

            switch (op_code) {
            case ARM_INS_AND:
                r = a & b;
                break;

            case ARM_INS_EOR:
                r = a ^ b;
                break;

            case ARM_INS_ORR:
                r = a | b;
                break;

            case ARM_INS_BIC:
                r = a & (~b);
                break;

            default:
                break;
            }

            Rx(rd) = r;

            if (thm->update_flags) {
                setN(r);
                setZ(r);
            }
            break;
        }

        case ARM_INS_STR:
        case ARM_INS_STRH:
        case ARM_INS_STRB:
        case ARM_INS_LDRH:
        case ARM_INS_LDRSH:
        case ARM_INS_LDRSB:
        case ARM_INS_LDRB:
        case ARM_INS_LDR: {
            int rt, rn, rm;
            uint32_t addr;
            arm_op_mem *mem;

            rt = oprnds[0].reg;
            mem = &oprnds[1].mem;
            rn = mem->base;
            rm = mem->index;

            /* TODO Pre/Post index addressing */

            if (rm) {
                /* LDR register */
                addr = Rx(rn) + Rx(rm) + mem->disp;
            } else {
                /* LDR imm */
                if (rn == ARM_REG_PC) {
                    /* PC relative */
                    addr = mem->disp + PC + 4;
                    addr &= ~3;
                } else {
                    addr = Rx(rn) + mem->disp;
                }
            }

            switch (op_code) {
            case ARM_INS_STR:
                SW(addr, Rx(rt));
                break;

            case ARM_INS_STRH:
                SH(addr, Rx(rt));
                break;

            case ARM_INS_STRB:
                SB(addr, Rx(rt));
                break;

            case ARM_INS_LDR:
                Rx(rt) = LW(addr);
                break;

            case ARM_INS_LDRH:
                Rx(rt) = LH(addr);
                break;

            case ARM_INS_LDRB:
                Rx(rt) = LB(addr);
                break;

            case ARM_INS_LDRSH:
                Rx(rt) = LH(addr);
                break;

            case ARM_INS_LDRSB:
                Rx(rt) = LB(addr);
                break;

            default:
                break;
            }
            break;
        }

        case ARM_INS_MOVS:
        case ARM_INS_MVN:
        case ARM_INS_MOV: {
            int rd;
            uint32_t r;

            rd = oprnds[0].reg;

            if (oprnds[1].type == ARM_OP_IMM) {
                /* OP Rx, #imm */
                r = oprnds[1].imm;
            } else if (oprnds[1].type == ARM_OP_REG) {
                r = Rx(oprnds[1].reg);
            }
            if (op_code == ARM_INS_MVN) {
                r = 0xffffffff ^ r;
            }

            Rx(rd) = r;

            if (thm->update_flags) {
                setN(r);
                setZ(r);
            }
            break;
        }

        case ARM_INS_ASR:
        case ARM_INS_LSR:
        case ARM_INS_LSL: {
            int rd, rm;
            uint32_t a, b, r = 0;

            rd = oprnds[0].reg;
            rm = oprnds[1].reg;

            if (op_cnt == 3) {
                a = Rx(rm);
                b = oprnds[2].imm;
            } else {
                a = Rx(rd);
                b = Rx(rm) & 0xff;
            }

            switch (op_code) {
            case ARM_INS_LSL:
                r = a << b;
                break;

            case ARM_INS_LSR:
                r = a >> b;
                break;

            case ARM_INS_ASR:
                r = a >> b;
                if (a & 0x80000000) {
                    r |= (~0) << (32 - b);
                }
                break;

            default:
                break;
            }

            Rx(rd) = r;

            if (thm->update_flags) {
                setN(r);
                setZ(r);

                if (op_code == ARM_INS_LSL)
                    C = !!(a & (1 << (32 - b)));
                else
                    C = !!(a & (1 << (b - 1)));
            }
            break;
        }

        case ARM_INS_SBC:
        case ARM_INS_RSB:
        case ARM_INS_MUL:
        case ARM_INS_ADD:
        case ARM_INS_ADC:
        case ARM_INS_SUB: {
            int rd, rn, rm;
            uint32_t a, b, r = 0;

            rd = oprnds[0].reg;

            if (op_cnt == 2) {
                a = Rx(rd);
                if (oprnds[1].type == ARM_OP_IMM) {
                    /* OP Rx, #imm */
                    b = oprnds[1].imm;
                } else if (oprnds[1].type == ARM_OP_REG) {
                    rn = oprnds[1].reg;
                    /* OP Rx, Rn */
                    b = Rx(rn);
                    if (rn == ARM_REG_PC)
                        b += 4;
                }
            } else {
                rn = oprnds[1].reg;
                a = Rx(rn);

                if (oprnds[2].type == ARM_OP_REG) {
                    /* OP Rd, Rn, Rm */
                    rm = oprnds[2].reg;
                    b = Rx(rm);
                } else {
                    /* OP Rd, Rn, imm */
                    b = oprnds[2].imm;
                }
            }

            switch (op_code) {
            case ARM_INS_ADD:
                r = a + b;
                break;

            case ARM_INS_ADC:
                b += C;
                r = a + b;
                break;

            case ARM_INS_SUB:
                r = a - b;
                break;

            case ARM_INS_SBC:
                b += !C;
                r = a - b;
                break;

            case ARM_INS_MUL:
                r = a * b;
                break;

            case ARM_INS_RSB:
                r = b - a;
                break;

            default:
                break;
            }

            if (thm->update_flags) {
                setN(r);
                setZ(r);
                switch (op_code) {
                case ARM_INS_ADD:
                    setC(a, b, 0);
                    setV(a, b, 0);
                    break;

                case ARM_INS_ADC:
                    if (C) {
                        setC(a, b, 1);
                        setV(a, b, 1);
                    } else {
                        setC(a, b, 0);
                        setV(a, b, 0);
                    }
                    break;

                case ARM_INS_SUB:
                    setC(a, ~b, 1);
                    setV(a, ~b, 1);
                    break;

                case ARM_INS_SBC:
                    if (C) {
                        setC(a, ~b, 1);
                        setV(a, ~b, 1);
                    } else {
                        setC(a, ~b, 0);
                        setV(a, ~b, 0);
                    }
                    break;

                case ARM_INS_RSB:
                    setC(b, ~a, 1);
                    setV(b, ~a, 1);
                    break;

                default:
                    break;
                }
            }

            Rx(rd) = r;

            break;
        }

        case ARM_INS_BL:
        case ARM_INS_BLX: {
            uint32_t addr;
            uint32_t lr;

            if (oprnds[0].type == ARM_OP_IMM) {
                addr = oprnds[0].imm;
                lr = PC + 4;
            } else {
                addr = Rx(oprnds[0].reg);
                lr = PC + 2;
            }

            addr &= ~1;
            /* This is fatal error */
            if (!addr) {
                PANIC("BL/BLX to NULL\n");
            }

            push_frame(elf_lookup_name(elf, addr), addr, lr);

            if (call_stub(ld, addr, lr) == 0) {
                /* no stub available */
                LR = lr;
                curr = addr;
                code = (uint8_t *) PA(curr);
                cs_free(insn, 1);
                insn = NULL;
            } else
                pop_frame();

            break;
        }

        case ARM_INS_BX:
        case ARM_INS_B: {
            uint32_t addr;

            if (oprnds[0].type == ARM_OP_IMM)
                addr = oprnds[0].imm;
            else
                addr = Rx(oprnds[0].reg);

            addr &= ~1;
            /* This is fatal error */
            if (!addr) {
                PANIC("B/BX to NULL\n");
            }
            if (cc_match(cc)) {
                curr = addr;
                code = (uint8_t *) PA(curr);
                cs_free(insn, 1);
                insn = NULL;
                if (op_code == ARM_INS_BX)
                    pop_frame();
            }
            break;
        }

        case ARM_INS_TST:
        case ARM_INS_TEQ:
        case ARM_INS_CMN:
        case ARM_INS_CMP: {
            int rn, rm;
            uint32_t a, b, r = 0;

            rn = oprnds[0].reg;
            a = Rx(rn);

            /* TODO shift */

            if (oprnds[1].type == ARM_OP_IMM) {
                b = oprnds[1].imm;
            } else {
                rm = oprnds[1].reg;
                b = Rx(rm);
            }
            switch (op_code) {
            case ARM_INS_CMP:
                r = a - b;
                setC(a, ~b, 1);
                setV(a, ~b, 1);
                break;

            case ARM_INS_CMN:
                r = a + b;
                setC(a, b, 0);
                setV(a, b, 0);
                break;

            case ARM_INS_TST:
                r = a & b;
                break;

            case ARM_INS_TEQ:
                r = a ^ b;
                break;

            default:
                break;
            }

            setZ(r);
            setN(r);

            break;
        }

        case ARM_INS_PUSH:
            for (i = op_cnt; i > 0; i--) {
                uint32_t r;

                if (SP <= VM_STACK_START) {
                    PANIC("Stack Overflow\n");
                }
                SP -= 4;
                r = Rx(oprnds[i - 1].reg);
                SW(SP, r);
            }
            break;

        case ARM_INS_POP:
            for (i = 0; i < op_cnt; i++) {
                uint32_t r;

                r = LW(SP);
                SP += 4;
                Rx(oprnds[i].reg) = r;

                if (oprnds[i].reg == ARM_REG_PC) {
                    cs_free(insn, 1);
                    insn = NULL;
                    if (r == VM_INVALID_ADDR) {
                        /* the outter most routine */
                        return;
                    }
                    r &= ~1;
                    curr = r;
                    PC = curr;
                    code = (uint8_t *) PA(curr);
                    pop_frame();
                }
            }
            break;

        case ARM_INS_LDM: {
            int rn, rx;
            uint32_t addr, val;

            rn = oprnds[0].reg;
            addr = Rx(rn);
            for (i = 1; i < op_cnt; i++) {
                val = LW(addr);
                rx = oprnds[i].reg;
                Rx(rx) = val;
                addr += 4;
            }
            if (thm->writeback)
                Rx(rn) = addr;
            break;
        }

        case ARM_INS_STM: {
            int rn, rx;
            uint32_t addr, val;

            rn = oprnds[0].reg;
            addr = Rx(rn);
            for (i = 1; i < op_cnt; i++) {
                rx = oprnds[i].reg;
                val = Rx(rx);
                SW(addr, val);
                addr += 4;
            }
            if (thm->writeback)
                Rx(rn) = addr;
            break;
        }

        case ARM_INS_NOP:
            break;

        default:
            PANIC("Unknown OP: %d\n", op_code);
        }

        if (insn) {
            code += insn->size;
            curr += insn->size;
            cs_free(insn, 1);
            insn = NULL;
        }

        if (stop_exec)
            return;

    } while (1);
}

int
arm_exec(struct armld *ld,
         uint32_t start,
         uint32_t halt,
         uint32_t *args,
         int cnt)
{
    int i;
    uint32_t sp;
    struct vm *vm = ld->vm;

    if (__exec_env.init == 0) {
        EXEC_ENV_INIT();
        if (cs_open(CS_ARCH_ARM,
                    CS_MODE_THUMB,
                    &__exec_env.cp) != CS_ERR_OK) {
            return -1;
        }
        cs_option(__exec_env.cp, CS_OPT_DETAIL, CS_OPT_ON);
        SP = VM_STACK_END;
        __exec_env.init = 1;
    }

    /* at least 4 args provided */
    if (cnt < 4)
        PANIC("At least 4 args required.\n");

    R0 = args[0];
    R1 = args[1];
    R2 = args[2];
    R3 = args[3];

    /* rest goes to stack */
    if (cnt > 4) {
        SP -= 4 * (cnt - 4);
        sp = SP;
        for (i = 4; i < cnt; i++) {
            SW(sp, args[i]);
            sp += 4;
        }
    }

    LR = VM_INVALID_ADDR;

    FMI = 0;
    sub_exec(__exec_env.cp, ld, start, halt);

    return R0;
}

void
exec_load_registers(uint32_t core[16], int cpsr[4])
{
    memcpy(core, __exec_env.core_registers, sizeof(__exec_env.core_registers));
    memcpy(cpsr, __exec_env.cpsr, sizeof(__exec_env.cpsr));
}

void
exec_set_r1(uint32_t r)
{
    R1 = r;
}

void
exec_set_breakpoint(uint32_t addr)
{
    exec_breakpoint = addr;
}
