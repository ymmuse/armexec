/*
 * This file is part of armexec.
 *
 * armexec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * armexec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with armexec.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _VM_H
#define _VM_H

#include <stdio.h>
#include <stdint.h>

struct vm;
struct elf;

/**
 * memory map
 *
 *
 * +----------------+ ---> stack end
 * |                | |
 * |                | v
 * +----------------+ ---> stack start
 * |                | ^
 * |                | |
 * +----------------+ ---> heap start
 * |                |
 * +----------------+ ---> elf start: 0
 */

#define VM_HEAP_SIZE   (1024*1024*16)
#define VM_STACK_SIZE  (1024*1024*1)

/* Assume elf is less than 16M */
#define ELF_FILE_MAX    (1024*1024*16)

#define VM_HEAP_START  ELF_FILE_MAX
#define VM_HEAP_END    (VM_HEAP_START + VM_HEAP_SIZE)
#define VM_STACK_START VM_HEAP_END
#define VM_STACK_END   (VM_STACK_START + VM_STACK_SIZE)

#define VM_INVALID_ADDR    0xdeadbeef

extern volatile uint32_t vm_mem_watch;
extern volatile int vm_mem_watch_type;

#define __vm_load(__type, __vm, __addr) ({ \
    __type *__p = (__type *)vm_phy_addr((__vm), (__addr)); \
    if ((__addr) == vm_mem_watch && vm_mem_watch_type == 0) \
        __breakpoint(); \
    __type __val = 0; \
    __val = *__p; \
    (__val); \
})

#define __vm_store(__type, __vm, __addr, __val) do { \
    __type *__p = (__type *)vm_phy_addr((__vm), (__addr)); \
    if ((__addr) == vm_mem_watch && vm_mem_watch_type == 1) \
        __breakpoint(); \
    *__p = (__val); \
} while(0)

#define LW(__addr) __vm_load(uint32_t, vm, __addr)
#define LH(__addr) __vm_load(uint16_t, vm, __addr)
#define LB(__addr) __vm_load(uint8_t, vm, __addr)

#define SW(__addr, __val) \
    __vm_store(uint32_t, vm, __addr, __val)

#define SH(__addr, __val) \
    __vm_store(uint16_t, vm, __addr, __val)

#define SB(__addr, __val) \
    __vm_store(uint8_t, vm, __addr, __val)

#define PA(__addr)  (vm_phy_addr(vm, (__addr)))

#define VSTR(s)     vm_malloc_string(vm, (char *)(s)) 

static inline void
vm_set_mem_watch(uint32_t addr, int type)
{
    vm_mem_watch = addr;
    vm_mem_watch_type = type;
}

void *
vm_phy_addr(struct vm *, uint32_t addr);

uint32_t
vm_malloc(struct vm *, uint32_t len, void **ph);

uint32_t
vm_malloc_string(struct vm *, char *s);

void
vm_set_elf(struct vm *, struct elf *);

struct vm *
vm_init(void);

#endif
