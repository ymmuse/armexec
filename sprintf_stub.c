/*
 * This file is part of armexec.
 *
 * armexec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * armexec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with armexec.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <ctype.h>

#include "armexec.h"
#include "plt_stub.h"

uint32_t
vsprintf_stub(int start,
              struct armld *ld,
              char *dst,
              char *fmt,
              uint32_t r0,
              uint32_t r1,
              uint32_t r2,
              uint32_t r3,
              uint32_t sp)
{
    struct vm *vm = ld->vm;
    char *orig = dst;
    char ch;
    int i = start - 1; /* index of args */
    char *s;
    uint32_t x;
    char local[16] = {0};

/* If i is in [0, 3], return value in register.
 * otherwise read from sp
 */
#define next_va() ({ \
    ++i; \
    uint32_t res; \
    switch (i) { \
    case 0: res = r0; break; \
    case 1: res = r1; break; \
    case 2: res = r2; break; \
    case 3: res = r3; break; \
    default: \
        res = LW(sp + (i - 4) * 4); \
        break; \
    } \
    (res); \
})

    while ((ch = *fmt++)) {
        if (ch != '%') {
            *dst++ = ch;
            continue;
        }
        ch = *fmt++;
        if (ch == '%') {
            *dst++ = ch;
            continue;
        }

        /* e.g. %02x */
        if (isdigit(ch)) {
            bzero(local, sizeof(local));
            s = local;
            *s++ = '%';
            *s++ = ch;
            while (isdigit((ch = *fmt++))) {
                *s++ = ch;
            }
            *s++ = ch;
            x = next_va();
            dst += sprintf(dst, local, x);
            continue;
        }

        switch (ch) {
        case 'd':
            x = next_va();
            dst += sprintf(dst, "%d", x);
            break;

        case 'x':
            x = next_va();
            dst += sprintf(dst, "%x", x);
            break;

        case 'X':
            x = next_va();
            dst += sprintf(dst, "%X", x);
            break;

        case 'u':
            x = next_va();
            dst += sprintf(dst, "%u", x);
            break;

        case 'c':
            x = next_va();
            dst += sprintf(dst, "%c", x);
            break;

        case 's':
            x = next_va();
            s = PA(x);
            dst += sprintf(dst, "%s", s);
            break;

        case 'p':
            x = next_va();
            dst += sprintf(dst, "%x", x);
            break;

        default:
            PANIC("Unknown format: %c\n", ch);
            break;
        }
    }

    *dst = 0;
    return dst - orig;
}

DCLR_STUB(sprintf)
{
    struct vm *vm = ld->vm;
    char *dst = PA(r0);
    char *fmt = PA(r1);

    return vsprintf_stub(2, ld, dst, fmt, r0, r1, r2, r3, sp);
}

DCLR_STUB(printf)
{
    struct vm *vm = ld->vm;
    char *dst = malloc(1024);
    char *fmt = PA(r0);

    vsprintf_stub(1, ld, dst, fmt, r0, r1, r2, r3, sp);
    printf("%s", dst);
    free(dst);

    return 0;
}
